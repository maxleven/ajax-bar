import gulp from 'gulp';
import ts from 'gulp-typescript';
import { rimraf } from 'rimraf';
import rename from 'gulp-rename';
import gulpSass from 'gulp-sass';
import dartSass from 'sass';
import postcss from 'gulp-postcss';
import cssnano from 'cssnano';
import autoprefixer from 'autoprefixer';

function clean(done) {
  rimraf('dist').finally(() => {
    done();
  });
}

const esmProject = ts.createProject('tsconfig.json', {
  noImplicitAny: true,
});

function esm() {
  return gulp
    .src('src/*.ts')
    .pipe(esmProject())
    .pipe(
      rename((path) => {
        path.basename += '.esm';
      }),
    )
    .pipe(gulp.dest('dist'));
}

const cjsProject = ts.createProject('tsconfig.json', {
  noImplicitAny: true,
  module: 'CommonJS',
});

function cjs() {
  return gulp
    .src('src/*.ts')
    .pipe(cjsProject())
    .pipe(
      rename((path) => {
        path.basename += '.cjs';
      }),
    )
    .pipe(gulp.dest('dist'));
}

const sass = gulpSass(dartSass);

function sassBuild() {
  const postCssPlugins = [autoprefixer(), cssnano()].filter(Boolean);

  return gulp
    .src(['src/scss/**.scss', '!src/scss/_*.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(postCssPlugins))
    .pipe(gulp.dest('dist/css'));
}

function watch() {
  gulp.watch('src/**/*.ts').on('all', gulp.parallel(esm, cjs));
  gulp.watch('src/scss/**/*.scss').on('all', sassBuild);
}

gulp.task('build', gulp.series(clean, gulp.parallel(esm, cjs, sassBuild)));
gulp.task('default', gulp.series('build', watch));
