# AjaxBar

## JS

```javascript
import AjaxBar from '@maxleven/ajax-bar';
const options = {
    element: document.getElementById('ajax-bar');
}
const ajaxBar = new AjaxBar(options);

// Show ajax bar
ajaxBar.show();

// Hide ajax bar
ajaxBar.hide();

// share instance to other files
export ajaxBar;

// add ajaxBar globally
// not recommended
window.ajaxBar = ajaxBar;
```

Options is not required

| Option         | value               | Data option                   | Defautl            |
| -------------- | ------------------- | ----------------------------- | ------------------ |
| `element`      | `HTMLElement`       | none                          | Create HTMLElement |
| `text`         | `string`            | `data-ajax-bar-text`          | `undefined`        |
| `textPosition` | `'top' \| 'bottom'` | `data-ajax-bar-text-position` | `'bottom'`         |

Data options have the highest priority

Example of using data options

```html
<div
  id="ajax-bar"
  data-ajax-bar-text="WTF"
  data-ajax-bar-text-position="top"
></div>
```

## CSS

To connect css you can use allready builded files from `dist/css` or source scss files from `src/scss`.

Css variables:

| Variable                | Default value |
| :---------------------- | :------------ |
| `--ajb-primary-color`   | #000          |
| `--ajb-secondary-color` | #00000020;    |
| `--ajb-gap`             | 16px;         |
| `--ajb-text-color`      | #000000;      |
| `--ajb-font-family`     | inherit;      |
| `--ajb-font-size`       | inherit;      |
| `--ajb-font-style`      | inherit;      |
| `--ajb-font-weight`     | inherit;      |
| `--ajb-bg-color`        | #ffffffee;    |
| `--ajb-bg-blur`         | 4px;          |
| `--ajb-bg-z-index`      | 2147483647;   |
| `--ajb-bg-transition`   | all 0.2s;     |

Available styles:

- [x] `circle-26`
- [x] `dots-5`
