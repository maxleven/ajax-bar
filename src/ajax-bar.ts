export interface AjaxBarOptions {
  element?: HTMLElement | undefined;
  text?: string | undefined;
  textPosition?: string;
}

export default class AjaxBar {
  element: HTMLElement;
  options: AjaxBarOptions;
  bar: HTMLElement;
  loader: HTMLElement;
  text: HTMLElement;

  constructor(options: AjaxBarOptions) {
    const defaults: AjaxBarOptions = {
      element: document.createElement('div'),
      text: undefined,
      textPosition: 'bottom',
    };
    const dataOptions: AjaxBarOptions = {
      text:
        options?.element?.dataset?.ajaxBarText ||
        options?.text ||
        defaults.text,
      textPosition:
        options?.element?.dataset?.ajaxBarTextPosition ||
        options?.textPosition ||
        defaults.textPosition,
    };
    this.options = Object.assign(defaults, options, dataOptions);

    if (!/^top|bottom$/g.test(this.options.textPosition)) {
      console.error(`Text posotion can be 'top' or 'bottom'`);
      this.options.textPosition = 'bottom';
    }

    this.bar = this.options.element;

    if (!this.bar) {
      throw new Error('Ajax bar element not found!');
    }

    this.loader = document.createElement('div');
    this.text = document.createElement('div');

    this.bar.classList.add('ajax-bar');
    this.loader.classList.add('ajax-bar-loader');
    this.text.classList.add('ajax-bar-text');

    this.bar.append(this.loader);

    if (this.options.text) {
      this.text.innerText = this.options.text;

      this.options.textPosition === 'bottom'
        ? this.bar.append(this.text)
        : this.bar.prepend(this.text);
    }

    document.body.append(this.bar);
  }

  show() {
    this.bar.classList.add('is-open');
  }

  hide() {
    this.bar.classList.remove('is-open');
  }
}
